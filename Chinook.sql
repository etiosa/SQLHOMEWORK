/*### 2.1 SELECT
* Task  Select all records from the Employee table.
* Task  Select all records from the Employee table where last name is King.
* Task  Select all records from the Employee table where first name is Andrew and REPORTSTO is NULL.*/
SELECT* FROM EMPLOYEE;
SELECT*FROM EMPLOYEE WHERE LASTNAME= 'King';
SELECT* FROM EMPLOYEE WHERE FIRSTNAME='Andrew' AND REPORTSTO IS NULL;


/*### 2.2 ORDER BY
* Task  Select all albums in Album table and sort result set in descending order by title.
* Task  Select first name from Customer and sort result set in ascending order by city */
SELECT * FROM ALBUM ORDER BY TITLE DESC;
SELECT FIRSTNAME FROM CUSTOMER ORDER BY CITY;

/*### 2.3 INSERT INTO
* Task  Insert two new records into Genre table
* Task  Insert two new records into Employee table
* Task  Insert two new records into Customer table */
SELECT* FROM GENRE;
INSERT INTO GENRE VALUES(26, 'AfroBeat');
INSERT INTO GENRE VALUES(27,'KPOP');

INSERT INTO EMPLOYEE VALUES(9,'James','John', 'IT Staff', 6, TO_DATE('17/12/2015','DD/MM/YYYY'),TO_DATE('17/12/2017','DD/MM/YYYY'), '870 courtlandt ave' , 'New York City','NY','USA', '10451', '+1 (888)-888-9899', '+1 (877)-657-9988','james@gmail.com');
INSERT INTO EMPLOYEE VALUES(10,'Ham','Jack', 'Sales Manager', 2, TO_DATE('17/12/2005','DD/MM/YYYY'),TO_DATE('1/12/2017','DD/MM/YYYY'), 'Jacks Height' , 'New York City','NY','USA', '10457', '+1 (885)-878-9899', '+1 (777)-6657-9088','Ham@gmail.com');

INSERT INTO CUSTOMER VALUES(60,'Okada','oba',null,'870 bayside', 'Sidney','NSW','AUSTRALIA', '2011', '+44 020 5678 6789',null,'okada@gmail.com',5);
INSERT INTO CUSTOMER VALUES(61,'jack','Hiro',null,'280 bayside ave', 'New York City','NY','America', '10451', '+1 718 7678 6789',null,'Hiro@gmail.com',6);


/*### 2.4 UPDATE
* Task  Update Aaron Mitchell in Customer table to Robert Walter
* Task  Update name of artist in the Artist table Creedence Clearwater Revival to CCR */
UPDATE CUSTOMER
SET FIRSTNAME = 'Robert',LASTNAME='Walter'
WHERE FIRSTNAME='Aaron' AND LASTNAME='Mitchell';

UPDATE ARTIST
SET NAME = 'CCR'
WHERE NAME='Creedence Clearwater Revival';

/*### 2.5 LIKE
* Task  Select all invoices with a billing address like T% */
SELECT *FROM INVOICE
WHERE BILLINGADDRESS LIKE('T%');

/*### 2.6 BETWEEN
* Task  Select all invoices that have a total between 15 and 50
* Task  Select all employees hired between 1st of June 2003 and 1st of March 2004 */
SELECT * FROM INVOICE
WHERE TOTAL BETWEEN 15 AND 50;

SELECT*FROM EMPLOYEE
WHERE HIREDATE BETWEEN '1-June-2003'AND '1-March-2004';

/*### 2.7 DELETE
* Task  Delete a record in Customer table where the name is Robert Walter (There may be constraints that rely on this, find out how to resolve them). */

ALTER TABLE INVOICE
DROP CONSTRAINT FK_INVOICECUSTOMERID;
DELETE FROM CUSTOMER
WHERE FIRSTNAME= 'Robert'AND LASTNAME='Walter';
 
/*### 3.1 System Defined Functions
* Task  Create a function that returns the current time.
* Task  create a function that returns the length of a mediatype from the mediatype table */





CREATE FUNCTION get_localtime
RETURN DATE
IS
mylocaldate DATE;
BEGIN
mylocaldate:= Current_Date;
RETURN mylocaldate;
END; 

/*dual is temporal table internal in sql */

/*select get_localtime from dual; */


CREATE FUNCTION get_medial_source_lengths(p_medial_type VARCHAR2)
RETURN NUMBER IS
my_media_length NUMBER;
medial_Type VARCHAR2(20);
BEGIN
 SELECT NAME  INTO medial_Type 
 FROM MEDIATYPE
 WHERE NAME=p_medial_Type;
my_media_length:= LENGTH(medial_type) ; 
 /* my_media_length:= COUNT(medial_type) ;*/

 RETURN my_media_length;
END;
/*Testing purpose */
select get_medial_source_lengths('MPEG audio file')  FROM MEDIATYPE
where name ='MPEG audio file';


/*### 3.2 System Defined Aggregate Functions
* Task  Create a function that returns the average total of all invoices
* Task  Create a function that returns the most expensive track */


CREATE FUNCTION get_invoce_Average
   RETURN NUMBER 
   IS invoice_average NUMBER;
   BEGIN 
    SELECT AVG(Total) 
    INTO invoice_average
      FROM INVOICE;
        RETURN invoice_average;
    END;
SELECT  get_invoce_Average from invoice;


CREATE FUNCTION get_invoce_max 
RETURN NUMBER IS
track_max NUMBER;
BEGIN
SELECT MAX(UnitPrice) "Maximum"
INTO track_max
  FROM TRACK;
RETURN track_max;
END;
SELECT get_invoce_max  FROM  track;


/*### 3.3 User Defined Scalar Functions
* Task  Create a function that returns the average price of invoiceline items in the invoiceline table */
CREATE FUNCTION get_average_pric_e
   RETURN NUMBER 
   IS avg_price NUMBER;
   BEGIN 
     SELECT AVG(UNITPRICE) "Average"
     INTO avg_price
       FROM INVOICELINE;
      
      RETURN avg_price;
    END;
   

select get_average_pric FROM INVOICELINE;
SELECT AVG(unitprice)FROM INVOICELINE;


/*### 3.4 User Defined Table Valued Functions
* Task  Create a function that returns all employees who are born after 1968.*/
CREATE  OR REPLACE TYPE t_COL AS OBJECT(fName VARCHAR2(20), lName VARCHAR2(20), BIRTHDATE DATE);
CREATE OR REPLACE TYPE result_Table AS TABLE of t_COL; /* create a table*/ 

CREATE OR REPLACE FUNCTION get_employess
RETURN result_Table PIPELINED 
IS 
BEGIN
FOR i IN  (SELECT FIRSTNAME,LASTNAME,BIRTHDATE
FROM EMPLOYEE
 WHERE TO_CHAR(TO_DATE(BIRTHDATE,'yyyy')) > '1968') loop
 PIPE ROW( t_COL(i.FIRSTNAME,i.LASTNAME,i.BIRTHDATE));
 END LOOP;
 
RETURN;

END;


/*### 4.1 Basic Stored Procedure
* Task  Create a stored procedure that selects the first and last names of all the employees.*/

CREATE OR REPLACE PROCEDURE EMPLYOEEPROC 
(
  OUT_FIRSTNAME OUT VARCHAR2 
, OUT_LASTNAME OUT VARCHAR2 
) AS 
BEGIN
  SELECT FIRSTNAME, LASTNAME INTO
  OUT_FIRSTNAME,OUT_LASTNAME 
  FROM EMPLOYEE;
END EMPLYOEEPROC;


/*### 4.2 Stored Procedure Input Parameters
* Task  Create a stored procedure that updates the personal information of an employee.
* Task  Create a stored procedure that returns the managers of an employee. */

CREATE OR REPLACE PROCEDURE updateEmployeePROC
( EMPLOYEE_ID IN NUMBER,
  Update_address IN VARCHAR2,
  Update_City IN VARCHAR2,
  UDPATE_STATE IN VARCHAR2,
  UDPATE_Country IN VARCHAR2,
  
  UDPATE_Postcode IN VARCHAR2,
  UDPATE_Phone IN VARCHAR2,
  UDPATE_FAX IN VARCHAR2,
  UDPATE_EMAIL IN VARCHAR)
AS
BEGIN 
UPDATE EMPLOYEE
SET  ADDRESS=Update_address , CITY=Update_City, STATE=UDPATE_STATE,COUNTRY=UDPATE_Country,
POSTALCODE=UDPATE_Postcode,PHONE= UDPATE_Phone,
FAX=UDPATE_FAX,EMAIL=UDPATE_EMAIL
WHERE EMPLOYEE_ID = EMPLOYEEID;
END updateEmployeePROC;

SELECT * FROM EMPLOYEE;


/*### 4.3 Stored Procedure Output Parameters
* Task  Create a stored procedure that returns the name and company of a customer.*/

CREATE OR REPLACE PROCEDURE Retrun_Cust_proc
(INPUT_Cust_id IN NUMBER,
 OUT_FIRSTNAME OUT VARCHAR2,
 OUT_LASTNAME OUT VARCHAR2,
 OUT_COMPANY_NAME OUT VARCHAR2
) 
AS
BEGIN
SELECT FIRSTNAME,LASTNAME,COMPANY INTO
  OUT_FIRSTNAME,OUT_LASTNAME,OUT_COMPANY_NAME
  FROM CUSTOMER
  WHERE INPUT_Cust_id =CUSTOMERID;
 END Retrun_Cust_proc;

/*# 5) Transactions
In this section you will be working with transactions. Transactions are usually nested within a stored procedure.

* Task  Create a transaction that given a invoiceId will delete that invoice (There may be constraints that rely on this, find out how to resolve them).
* Task  Create a transaction nested within a stored procedure that inserts a new record in the Customer table */
CREATE OR REPLACE PROCEDURE Transaction_proc
( INPUT_INVOICE_ID IN NUMBER
)
/* orphan record..no reference to that  */
AS
BEGIN 
SAVEPOINT start_trans;
DELETE FROM INVOICELINE
WHERE INVOICEID=INPUT_INVOICE_ID;

DELETE FROM INVOICE
WHERE INVOICEID=INPUT_INVOICE_ID;
EXCEPTION 
WHEN others THEN
    ROLLBACK TO start_trans;
COMMIT;
END;

CREATE OR REPLACE PROCEDURE Customer_proc_tran
( Update_customerID IN NUMBER,
 Update_FirstName IN VARCHAR2,
 Update_LastName IN VARCHAR2,
 Update_Company IN VARCHAR2,
 Update_Address IN VARCHAR2,
 Update_City IN VARCHAR2,
 Update_State IN VARCHAR2,
 Update_Country IN VARCHAR2,
 Update_PostalCode IN VARCHAR2,
 Update_Phone IN VARCHAR2,
 Update_Fax IN VARCHAR2,
 Update_Email IN VARCHAR2
)
AS
BEGIN
SAVEPOINT save_record;
INSERT INTO CUSTOMER VALUES(Update_customerID ,Update_FirstName,Update_LastName,Update_Company,Update_Address,Update_City,Update_State,Update_Country ,
Update_PostalCode, Update_Phone,Update_Fax ,Update_Fax,Update_Email);
EXCEPTION 
WHEN dup_val_on_index THEN
ROLLBACK TO save_record;
COMMIT;
END;


/*### 6.1 AFTER/FOR
* Task - Create an after insert trigger on the employee table fired after a new record is inserted into the table.
* Task  Create an after update trigger on the album table that fires after a row is inserted in the table
* Task  Create an after delete trigger on the customer table that fires after a row is deleted from the table.*/

CREATE OR REPLACE TRIGGER Employee_after_insert
AFTER INSERT  ON  EMPLOYEE
FOR EACH ROW 
BEGIN 
INSERT INTO EMPLOYEE VALUES(:new.EMPLOYEEID,:new.LASTNAME,:new.FIRSTNAME,:new.TITLE,:new.REPORTSTO,:new.BIRTHDATE,:new.HIREDATE,:new.ADDRESS,:new.CITY,
:new.STATE, :new.COUNTRY, :new.POSTALCODE, :new.PHONE,:new.FAX,:new.EMAIL);
DBMS_OUTPUT.PUT_LINE('SUCESSFUL INSERT NEW RECORD INTO THE EMPLYOEE TABLE');
END;

CREATE OR REPLACE TRIGGER Album_after_insert
AFTER INSERT ON ALBUM
FOR EACH ROW 
BEGIN
DBMS_OUTPUT.PUT_LINE('SUCCESSFUL INSERT NEW RECORD INTO THE ALBUM TABLE');
END;

CREATE OR REPLACE TRIGGER CUstomer_after_Delete
AFTER DELETE ON CUSTOMER
FOR EACH ROW
BEGIN 
DBMS_OUTPUT.PUT_LINE('SUCCESSFUL DELETED FROM CUSTOMER TABLE');


/*### 7.1 INNER
* Task  Create an inner join that joins customers and orders and specifies the name of the customer and the invoiceId.*/
SELECT DISTINCT cus.Firstname,cus.Lastname, inv.invoiceid
FROM Customer cus 
INNER JOIN INVOICE inv
ON cus.CustomerId=inv.CustomerId;
/*### 7.2 OUTER
* Task  Create an outer join that joins the customer and invoice table, specifying the CustomerId, firstname, lastname, invoiceId, and total.*/
SELECT DISTINCT cus.CustomerId, cus.Firstname,cus.Lastname, inv.invoiceId,inv.Total
FROM CUSTOMER cus
FULL OUTER JOIN INVOICE inv
ON cus.CustomerId=inv.CustomerId;

/*### 7.3 RIGHT
* Task  Create a right join that joins album and artist specifying artist name and title. */
SELECT DISTINCT art.Name, al.Title 
FROM ARTIST art
RIGHT JOIN ALBUM al
ON art.ArtistId = al.ArtistId;


/*### 7.4 CROSS
* Task  Create a cross join that joins album and artist and sorts by artist name in ascending order.*/
SELECT * FROM ALBUM al
CROSS JOIN ARTIST art
ORDER BY art.NAME;

/*### 7.5 SELF
* Task  Perform a self-join on the employee table, joining on the reportsto column. */
SELECT e1.FirstName,e1.LastName,e2.REPORTSTO FROM EMPLOYEE e1
JOIN EMPLOYEE e2
ON  e1.EmployeeId =e2.REPORTSTO
WHERE  e2.REPORTSTO IS  NOT NULL ;


/*SELECT TO_CHAR(TO_DATE('22-AUG-03'), 'yyyy') FROM dual;*/
